# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANT_ROOT = File.dirname(File.expand_path(__FILE__))

Vagrant.configure("2") do |config|
  config.vm.box = "debian/buster64"
  config.vm.synced_folder ".", "/vagrant", disabled: true
  config.vm.provider "virtualbox" do |v|
    v.cpus = 1
    v.memory = 256
    v.check_guest_additions = false
  end

  config.vm.define "client" do |client|
    client.vm.hostname = "client"
    client.vm.network "private_network", ip: "192.168.120.101"
  end

  NB_NODES = 3
  NODES_NAMES = []
  NODES_IPS = []
  (1..NB_NODES).each do |i|
    disk = File.join(VAGRANT_ROOT, "disk-data-gfs#{i}.vdi")
    name = "gfs#{i}"
    NODES_NAMES.push(name)
    ip = "192.168.120.1#{i}"
    NODES_IPS.push(ip)
    config.vm.define name do |gfs|
      gfs.vm.hostname = name
      gfs.vm.network "private_network", ip: ip
      gfs.vm.provider "virtualbox" do |v|
        unless File.exist?(disk)
          v.customize ['createmedium', 'disk', '--filename', disk, '--variant', 'Fixed', '--size', 4 * 1024]
        end
        v.customize ['storageattach', :id,  '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disk]
      end
      if i == NB_NODES
        gfs.vm.provision "ansible" do |ansible|
          #ansible.verbose = "vvv"
          ansible.become = true
          ansible.groups = {
            "glusterfs_client" => ["client"],
            "glusterfs_server" => NODES_NAMES,
          }
          ansible.limit = "all"
          ansible.playbook = "site.yml"
          ansible.extra_vars = {
            "lvm_vg_devices": "/dev/sdb",
            "lvm_lv_size": "3G",
            "fs_device": "/dev/mapper/data--vg-thin--vol",
            "gfsd_peers": NODES_IPS,
            "gfsd_disperses": "{{ gfsd_peers | length }}",
            "gfsd_options": {
              "network.ping-timeout": "5",
              "auth.allow": "192.168.120.*",
            },
            "gfs_mount_src": "{{ gfsd_peers[0] }}:/gv0",
          }
        end
      end
    end
  end
end
