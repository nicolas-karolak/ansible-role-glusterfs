#!/usr/bin/python3

import argparse
from pathlib import Path
import random
import subprocess
import time
import uuid


def setup_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-p', '--path',
        help='path where random files wille be created (default: /data/)',
        type=str,
        default='/data/',
        )
    parser.add_argument(
        '-n', '--nb-files',
        help='maximum number of files to create (default: 15)',
        type=int,
        default=15,
        )
    parser.add_argument(
        '-s', '--sleep',
        help='time to wait between files creation, in seconds (default: 1)',
        type=int,
        default=1,
        )
    parser.add_argument(
        '-i', '--min-size',
        help='minimum file size, in KB (default: 10)',
        type=int,
        default=10,
        )
    parser.add_argument(
        '-a', '--max-size',
        help='maximum file size, in KB (default: 1000000)',
        type=int,
        default=1000000,
        )

    return parser.parse_args()


def write_file(name: str, size: int):
    cmd = [
        '/usr/bin/dd',
        'if=/dev/urandom',
        f'of={name}',
        'bs=2014',
        f'count={size}',
        ]
    subprocess.run(cmd)


def list_files(path: str) -> list:
    return list(Path(path).iterdir())


def remove_random_file(files: list):
    random_id = random.randrange(0, len(files))
    Path(files[random_id]).unlink()


def main():
    args = setup_args()

    while True:
        # ensure that there is no more than expected
        files = list_files(args.path)
        if len(files) >= args.nb_files:
            remove_random_file(files)

        # create a random named and sized file
        name = args.path + uuid.uuid4().hex
        size = random.randint(args.min_size, args.max_size)
        write_file(name, size)

        time.sleep(args.sleep)


if __name__ == '__main__':
    main()
