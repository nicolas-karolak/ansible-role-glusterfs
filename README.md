# GlusterFS

## Requirements

- Ansible >= 2.10.x
- VirtualBox
- Vagrant

## Usage

Start VMs:

```
$ vagrant up
```

Export SSH config for instancied VMs:

```
$ vagrant ssh-config >> ~/.ssh/config
```

Deploy GlusterFS:

```
$ ansible-playbook site.yml
```

Extend GlusterFS cluster:

```
$ git checkout add-peers
$ ansible-playbook site.yml -e gfsd_rebalance=true
```
