# TODOs

## Configuration

- [x] LVM thin
- [ ] pare-feu

## Test

- [x] continuité lors de l'échec d'un nœud
- [x] réintégration d'un nœud
- [ ] remplacement d'un nœud
- [ ] mise à jour du cluster 
- [x] snapshots
- [ ] geo-replication
- [ ] bench avec fio
- [ ] bench avec locust

## Resources

<https://arstechnica.com/gadgets/2020/02/how-fast-are-your-disks-find-out-the-open-source-way-with-fio/>
